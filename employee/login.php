<?php
include('../admin/config.php');
$msg='';
if(isset($_POST['submit'])) {
	$user_id = $_POST['user_id'];
	$date_birth = $_POST['date_birth'];
    $user_id =intval($user_id );
	$query = "select * from `Priority_users` where `user_id`='$user_id' AND `date_birth`='$date_birth'";
	$result = mysqli_query($con,$query);
	if(mysqli_num_rows($result)>0){
		$admin=mysqli_fetch_assoc($result);
		$_SESSION['employee_sess']=$admin;
		header("location:index.php");
	}else{
		$msg = "<div style='background-color:red; height:6%; line-height:2; color:#fff; font-weight:bold; font-size:18px; width:60%;direction: rtl;'> מספר זהות או שנת לידה לא תקינים!</div>";
	}
}
?>


<!DOCTYPE html>
<html lang="he">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Login</title>

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="assets/css/form-elements.css">
        <link rel="stylesheet" href="assets/css/style_.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="assets/ico/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">

    </head>

    <body>

        <!-- Top content -->
        <div class="top-content">
        	
            <div class="inner-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2 text">
                            <h1> CFL מערכת לומדות </h1>
							
                            <div class="description">
                            	
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3 form-box">
                        	<div class="form-top">
                        		<div class="form-top-left">
                        			<h3>התחברות עובדים</h3>
									
                            		<p> :הכנס/י את מספר תעודת הזהות ושנת לידה</p>
                        		</div>
                        		<div class="form-top-right">
                        			<i class="fa fa-lock"></i>
                        		</div>
                            </div>
							
                            <div class="form-bottom">
							 <?php if($msg!=''){ echo $msg; } ?>
						
			                    <form action="" method="post" class="login-form">
			                    	<div class="form-group">
			                    		<label class="sr-only" for="form-username">Employee ID</label>
			                        	<input type="text" name="user_id" placeholder="הכנס תעודת זהות" class="form-username form-control" id="form-username">
			                        </div>
			                        <div class="form-group">
			                        	<label class="sr-only" for="form-password">Date of Birth</label>
			                        	<input type="text" name="date_birth" placeholder="שנת לידה" class="form-password form-control" id="form-password">
			                        </div>
									<div class="form-group">
									 <button class="btn btn-danger btn-block form_btn_"
									type="submit" name="submit" class="btn">היכנס/י</button><br><br>
			                       
	  
									</div>
								 </div><br>
        <span class="bigger-120">
							<span class="blue bolder">Tom Wininger & Rotem Zecharya</span>
							Application &copy; 2016-2017
						</span>
			                    </form>
		                    </div>
                        </div>
                    </div>
                  
                </div>
            </div>
            
        </div>


        <!-- Javascript -->
        <script src="assets/js/jquery-1.11.1.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/js/jquery.backstretch.min.js"></script>
        <script src="assets/js/scripts.js"></script>
        
        <!--[if lt IE 10]>
            <script src="assets/js/placeholder.js"></script>
        <![endif]-->

    </body>

</html>