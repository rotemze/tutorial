<?php
include("header.php");
$user_id = $_SESSION['employee_sess']['user_id'];
$user_type = $_SESSION['employee_sess']['role'];
?>

		<div class="main-container ace-save-state" id="main-container">
			<script type="text/javascript">
				try{ace.settings.loadState('main-container')}catch(e){}
			</script>

			<div id="sidebar" class="sidebar                  responsive                    ace-save-state">
				<script type="text/javascript">
					try{ace.settings.loadState('sidebar')}catch(e){}
				</script>

				<div class="sidebar-shortcuts" id="sidebar-shortcuts">
					<div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
						<!--<button class="btn btn-success">
							<i class="ace-icon fa fa-signal"></i>
						</button>

						<button class="btn btn-info">
							<i class="ace-icon fa fa-pencil"></i>
						</button>

						<button class="btn btn-warning">
							<i class="ace-icon fa fa-users"></i>
						</button>

						<button class="btn btn-danger">
							<i class="ace-icon fa fa-cogs"></i>
						</button>-->
					</div>

					<div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
						<!--<span class="btn btn-success"></span>

						<span class="btn btn-info"></span>

						<span class="btn btn-warning"></span>

						<span class="btn btn-danger"></span>-->
					</div>
				</div><!-- /.sidebar-shortcuts -->


				<ul class="nav nav-list">
					<li class="">
						<a href="index.php">
							<i class="menu-icon fa fa-tachometer"></i>
							<span class="menu-text"> ראשי </span>
						</a>

						<b class="arrow"></b>
					</li>
					
					
					
					<li class="">
						<a href="tables.php">
							<i class="menu-icon fa fa-list"></i>
							<span class="menu-text">לומדות</span>
						</a>

						<b class="arrow"></b>
					</li>
					
				</ul><!-- /.nav-list -->


				
				<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
					<i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
				</div>
			</div>

			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<!--<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="#">בית</a>
							</li>

							<li>
								<a href="#">חבילות למידה</a>
							</li>
							<li class="active">Simple &amp; Dynamic</li>
						</ul> /.breadcrumb -->

						<div class="nav-search" id="nav-search">
							<form class="form-search">
								<!--<span class="input-icon">
									<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
									<i class="ace-icon fa fa-search nav-search-icon"></i>
								</span>-->
							</form>
						</div><!-- /.nav-search -->
					</div>

					<div class="page-content">
						<div class="ace-settings-container" id="ace-settings-container">
							<div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
								<i class="ace-icon fa fa-cog bigger-130"></i>
							</div>

							<div class="ace-settings-box clearfix" id="ace-settings-box">
								<div class="pull-left width-50">
									<div class="ace-settings-item">
										<div class="pull-left">
											<select id="skin-colorpicker" class="hide">
												<option data-skin="no-skin" value="#438EB9">#438EB9</option>
												<option data-skin="skin-1" value="#222A2D">#222A2D</option>
												<option data-skin="skin-2" value="#C6487E">#C6487E</option>
												<option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
											</select>
										</div>
										<span>&nbsp; Choose Skin</span>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-navbar" autocomplete="off" />
										<label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-sidebar" autocomplete="off" />
										<label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-breadcrumbs" autocomplete="off" />
										<label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl" autocomplete="off" />
										<label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-add-container" autocomplete="off" />
										<label class="lbl" for="ace-settings-add-container">
											Inside
											<b>.container</b>
										</label>
									</div>
								</div><!-- /.pull-left -->

								<div class="pull-left width-50">
									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover" autocomplete="off" />
										<label class="lbl" for="ace-settings-hover"> Submenu on Hover</label>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact" autocomplete="off" />
										<label class="lbl" for="ace-settings-compact"> Compact Sidebar</label>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight" autocomplete="off" />
										<label class="lbl" for="ace-settings-highlight"> Alt. Active Item</label>
									</div>
								</div><!-- /.pull-left -->
							</div><!-- /.ace-settings-box -->
						</div><!-- /.ace-settings-container -->

						<div class="page-header">
							<!--<h1>
								חבילות למידה
								<small>
									<i class="ace-icon fa fa-angle-double-right"></i>
									Static &amp; Dynamic Tables
								</small>
							</h1>-->
						</div><!-- /.page-header -->

						<div class="row">
			<?php   
			$i=1;
			$j=1;
			$user_type1='blu_'.$user_type;
			$user_type2=$user_type;
			$query= "select * from `tbl_pkg` where b_basic='$user_type1' || b_CEO='$user_type1' || b_hop='$user_type1' || b_hrad_HR='$user_type1' || b_sm='$user_type1' || b_itm='$user_type1' || b_pw='$user_type1' || b_p2='$user_type1' || CFL_MANAGER='$user_type2' || MOKED_MANAGER='$user_type2' || CFL_DEP_MANAGER='$user_type2' || ROKAHAT_MOKED='$user_type2' || NURSE_MANAGER='$user_type2' || DATA_MANAGER='$user_type2' || REPRESENTATIVE='$user_type2' || FIELD_NURSE='$user_type2' order by id desc";
			 $result=mysqli_query($con,$query);
			 while($row=mysqli_fetch_assoc($result)){
			 ?>
						<div class="col-xs-12">
										<div class="row mange">
											<div class="col-sm-4">
											<?php 
											$qdid = $row['id'];
												$query_quer = "select * from `tbl_pkgs` where `logo_id`='$qdid'";
													$res_quer = mysqli_query($con, $query_quer);
													$row_quer = mysqli_fetch_array($res_quer);
													$question_id = $row_quer['id'];
												$query_bt = "select * from `result` where `user_id`='$user_id' and `question_id`='$question_id'";
												$result_sb = mysqli_query($con, $query_bt);
												$row_sb = mysqli_fetch_array($result_sb);
												// if(mysqli_num_rows($result_sb)>0){
											?>
												<!--a href="#" type="submit" name="submit" disabled class="btn btn-success">Read a Test</a-->
												<?php // }else{ ?>
												<a href="treeview.php?idl=<?php echo $row['id']; ?>" type="submit" name="submit" class="btn btn-success">קרא והיבחן</a>
												<?php //} ?>
											</div>
										  

											<div class="col-sm-4">
											 <br><br>
											 
											 <table class="table table-striped" style="margin-top: -26px;">
												<tr>
													<th>המבחן הבא</th>
													<th>סוג</th>
													<th>סטטוס</th>
												</tr>
												<?php 
													$qdid = $row['id'];
											$query_st = "select * from `PARY_LOADWORKERGRADE` where `IDNUMBER`='$user_id' and `PACKAGEID`='$qdid'order by `LINE` DESC limit 1";
												$result_st = mysqli_query($con, $query_st);
												$row_st = mysqli_fetch_array($result_st);
												if(mysqli_num_rows($result_st)>0){
												
													// echo count($result_st);
													$resulted_per = $row_st['GRADE'];
											 ?>
												<tr>
													<!--<td><?php echo date('d/m/Y',strtotime($row_st['UDATE'])); ?></td>-->
													<td><?php if($resulted_per >= 60){ echo date('d/m/Y',strtotime('+1 year',strtotime($row_st['UDATE'])));} if($resulted_per <= 60){echo date('d/m/Y',strtotime('+3 month',strtotime($row_st['UDATE']))); }?></td>
													<td><?php echo $user_type ?></td>
													<td><?php if($resulted_per >= 60){ echo '<span class="text-success">עבר</span>'; }if($resulted_per <= 60){ echo '<span class="text-danger">נכשל</span>'; } ?></td>
												</tr>
												<?php }?>
											 </table>
											</div>
											 <div class="col-sm-4"><label class="pull-right" style="font-size:20px;"><?php echo $j++;?></label><label class="pull-right" style="font-size:20px;">-</label><label class="pull-right" style="font-size:20px;"><?php echo $row['name_work'];?></label></div>
										</div><!---Mango-->
										
									</div><!-- /.span -->
			 <?php $i++;} ?>
									
								</div><!-- /.row -->
					<div class="form-group col-xs-12">
						<input type="submit" hidden name="total_marking" />
						<!--input type="submit" class="btn btn-warning pull-right" name="total_marking" data-toggle="modal" data-target="#myModal" value="Percentage of given answers" --/>
					</div>
					</div><!-- /.page-content -->
				</div>
				
				<!-- Trigger the modal with a button -->

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog" style="margin-top: 130px;">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <!--<h3 class="modal-title text-center">Your Test Results</h3>-->
		<h3 class="modal-title text-center">תוצאות הבחינה שלך</h3>
      </div>
      <div class="modal-body">
			<?php
					$query_stt = "select * from `result` where `user_id`='$user_id'";
					$res_stt = mysqli_query($con, $query_stt);
					$res2 = mysqli_num_rows($res_stt);
					
					$query_stt2 = "select * from `result` where `user_id`='$user_id' AND `result`='True'";
					$res_stt2 = mysqli_query($con, $query_stt2);
					$res_t = mysqli_num_rows($res_stt2);
					
					$total = (($res_t/$res2)*100);
					$i=1;
					$j=1;
			$user_type1='blu_'.$user_type;
			$user_type2=$user_type;
			$query= "select * from `tbl_pkg` where b_basic='$user_type1' || b_CEO='$user_type1' || b_hop='$user_type1' || b_hrad_HR='$user_type1' || b_sm='$user_type1' || b_itm='$user_type1' || b_pw='$user_type1' || b_p2='$user_type1' || CFL_MANAGER='$user_type2' || MOKED_MANAGER='$user_type2' || CFL_DEP_MANAGER='$user_type2' || ROKAHAT_MOKED='$user_type2' || NURSE_MANAGER='$user_type2' || DATA_MANAGER='$user_type2' || REPRESENTATIVE='$user_type2' || FIELD_NURSE='$user_type2'";
			 $result=mysqli_query($con,$query);
			 $row=mysqli_fetch_assoc($result);
						$total_assign = mysqli_num_rows($result);
						if($total_assign != 0){
			?>
			<!--<h2 class="text-center">Your Test Percentage till now is <?php echo $total; ?>%</h2>-->
			<h2 class="text-center">אחוז הבדיקה של עד עכשיו הוא<?php echo $total; ?>%</h2>
			<!--<h4 class="text-warning text-center">Please complete your full test</h4>-->
			<h4 class="text-warning text-center">אנא המשך בבקשה את שאר המבחן</h4>
			
					<?php }else{ ?>
			<!--<h2 class="text-center">Your Test Percentage is <?php echo $total; ?>%</h2>-->
			<h2 class="text-center">אחוז הבדיקה של הוא<?php echo $total; ?>%</h2>
							
						<?php 
						if($total>=60){
					// echo '<h4 class="text-success text-center">You have Passed the Test with average percentage. Your eligible for the next test after 1 year.</h4>';
					echo '<h4 class="text-success text-center">.עברת את הבחינה בהצלחה! מועד הבחינה הבא במבחן זה הנו בעוד שנה</h4>';
					 }else{
					// echo '<h4 class="text-danger text-center">You have Failed the Test. Your eligible for the next test after 3 months.</h4>';
					echo '<h4 class="text-danger text-center">!היית קרוב/ה מועד הבחינה הבא בעוד 3 חודשים.</h4>';
			}
								}
			?>
      </div>
    </div>

  </div>
</div>
				
			</div><!-- /.main-content -->

			<div class="footer">
				<div class="footer-inner">
					<div class="footer-content">
						<span class="bigger-120">
							<span class="blue bolder">Tom Wininger & Rotem Zecharya</span>
							Application &copy; 2017
						</span>

<!-- 						&nbsp; &nbsp;
 -->					<!-- 	<span class="action-buttons">
							<a href="#">
								<i class="ace-icon fa fa-twitter-square light-blue bigger-150"></i>
							</a>

							<a href="#">
								<i class="ace-icon fa fa-facebook-square text-primary bigger-150"></i>
							</a>

							<a href="#">
								<i class="ace-icon fa fa-rss-square orange bigger-150"></i>
							</a> -->
						</span>
					</div>
				</div>
			</div>

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

		<!-- basic scripts -->

		<!--[if !IE]> -->
		<script src="assets/js/jquery-2.1.4.min.js"></script>

		<!-- <![endif]-->

		<!--[if IE]>
<script src="assets/js/jquery-1.11.3.min.js"></script>
<![endif]-->
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		<script src="assets/js/bootstrap.min.js"></script>

		<!-- page specific plugin scripts -->
		<script src="assets/js/jquery.dataTables.min.js"></script>
		<script src="assets/js/jquery.dataTables.bootstrap.min.js"></script>
		<script src="assets/js/dataTables.buttons.min.js"></script>
		<script src="assets/js/buttons.flash.min.js"></script>
		<script src="assets/js/buttons.html5.min.js"></script>
		<script src="assets/js/buttons.print.min.js"></script>
		<script src="assets/js/buttons.colVis.min.js"></script>
		<script src="assets/js/dataTables.select.min.js"></script>

		<!-- ace scripts -->
		<script src="assets/js/ace-elements.min.js"></script>
		<script src="assets/js/ace.min.js"></script>

		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($) {
				//initiate dataTables plugin
				var myTable = 
				$('#dynamic-table')
				//.wrap("<div class='dataTables_borderWrap' />")   //if you are applying horizontal scrolling (sScrollX)
				.DataTable( {
					bAutoWidth: false,
					"aoColumns": [
					  { "bSortable": false },
					  null, null,null, null, null,
					  { "bSortable": false }
					],
					"aaSorting": [],
					
					
					//"bProcessing": true,
			        //"bServerSide": true,
			        //"sAjaxSource": "http://127.0.0.1/table.php"	,
			
					//,
					//"sScrollY": "200px",
					//"bPaginate": false,
			
					//"sScrollX": "100%",
					//"sScrollXInner": "120%",
					//"bScrollCollapse": true,
					//Note: if you are applying horizontal scrolling (sScrollX) on a ".table-bordered"
					//you may want to wrap the table inside a "div.dataTables_borderWrap" element
			
					//"iDisplayLength": 50
			
			
					select: {
						style: 'multi'
					}
			    } );
			
				
				
				$.fn.dataTable.Buttons.defaults.dom.container.className = 'dt-buttons btn-overlap btn-group btn-overlap';
				
				new $.fn.dataTable.Buttons( myTable, {
					buttons: [
					  {
						"extend": "colvis",
						"text": "<i class='fa fa-search bigger-110 blue'></i> <span class='hidden'>Show/hide columns</span>",
						"className": "btn btn-white btn-primary btn-bold",
						columns: ':not(:first):not(:last)'
					  },
					  {
						"extend": "copy",
						"text": "<i class='fa fa-copy bigger-110 pink'></i> <span class='hidden'>Copy to clipboard</span>",
						"className": "btn btn-white btn-primary btn-bold"
					  },
					  {
						"extend": "csv",
						"text": "<i class='fa fa-database bigger-110 orange'></i> <span class='hidden'>Export to CSV</span>",
						"className": "btn btn-white btn-primary btn-bold"
					  },
					  {
						"extend": "excel",
						"text": "<i class='fa fa-file-excel-o bigger-110 green'></i> <span class='hidden'>Export to Excel</span>",
						"className": "btn btn-white btn-primary btn-bold"
					  },
					  {
						"extend": "pdf",
						"text": "<i class='fa fa-file-pdf-o bigger-110 red'></i> <span class='hidden'>Export to PDF</span>",
						"className": "btn btn-white btn-primary btn-bold"
					  },
					  {
						"extend": "print",
						"text": "<i class='fa fa-print bigger-110 grey'></i> <span class='hidden'>Print</span>",
						"className": "btn btn-white btn-primary btn-bold",
						autoPrint: false,
						message: 'This print was produced using the Print button for DataTables'
					  }		  
					]
				} );
				myTable.buttons().container().appendTo( $('.tableTools-container') );
				
				//style the message box
				var defaultCopyAction = myTable.button(1).action();
				myTable.button(1).action(function (e, dt, button, config) {
					defaultCopyAction(e, dt, button, config);
					$('.dt-button-info').addClass('gritter-item-wrapper gritter-info gritter-center white');
				});
				
				
				var defaultColvisAction = myTable.button(0).action();
				myTable.button(0).action(function (e, dt, button, config) {
					
					defaultColvisAction(e, dt, button, config);
					
					
					if($('.dt-button-collection > .dropdown-menu').length == 0) {
						$('.dt-button-collection')
						.wrapInner('<ul class="dropdown-menu dropdown-light dropdown-caret dropdown-caret" />')
						.find('a').attr('href', '#').wrap("<li />")
					}
					$('.dt-button-collection').appendTo('.tableTools-container .dt-buttons')
				});
			
				////
			
				setTimeout(function() {
					$($('.tableTools-container')).find('a.dt-button').each(function() {
						var div = $(this).find(' > div').first();
						if(div.length == 1) div.tooltip({container: 'body', title: div.parent().text()});
						else $(this).tooltip({container: 'body', title: $(this).text()});
					});
				}, 500);
				
				
				
				
				
				myTable.on( 'select', function ( e, dt, type, index ) {
					if ( type === 'row' ) {
						$( myTable.row( index ).node() ).find('input:checkbox').prop('checked', true);
					}
				} );
				myTable.on( 'deselect', function ( e, dt, type, index ) {
					if ( type === 'row' ) {
						$( myTable.row( index ).node() ).find('input:checkbox').prop('checked', false);
					}
				} );
			
			
			
			
				/////////////////////////////////
				//table checkboxes
				$('th input[type=checkbox], td input[type=checkbox]').prop('checked', false);
				
				//select/deselect all rows according to table header checkbox
				$('#dynamic-table > thead > tr > th input[type=checkbox], #dynamic-table_wrapper input[type=checkbox]').eq(0).on('click', function(){
					var th_checked = this.checked;//checkbox inside "TH" table header
					
					$('#dynamic-table').find('tbody > tr').each(function(){
						var row = this;
						if(th_checked) myTable.row(row).select();
						else  myTable.row(row).deselect();
					});
				});
				
				//select/deselect a row when the checkbox is checked/unchecked
				$('#dynamic-table').on('click', 'td input[type=checkbox]' , function(){
					var row = $(this).closest('tr').get(0);
					if(this.checked) myTable.row(row).deselect();
					else myTable.row(row).select();
				});
			
			
			
				$(document).on('click', '#dynamic-table .dropdown-toggle', function(e) {
					e.stopImmediatePropagation();
					e.stopPropagation();
					e.preventDefault();
				});
				
				
				
				//And for the first simple table, which doesn't have TableTools or dataTables
				//select/deselect all rows according to table header checkbox
				var active_class = 'active';
				$('#simple-table > thead > tr > th input[type=checkbox]').eq(0).on('click', function(){
					var th_checked = this.checked;//checkbox inside "TH" table header
					
					$(this).closest('table').find('tbody > tr').each(function(){
						var row = this;
						if(th_checked) $(row).addClass(active_class).find('input[type=checkbox]').eq(0).prop('checked', true);
						else $(row).removeClass(active_class).find('input[type=checkbox]').eq(0).prop('checked', false);
					});
				});
				
				//select/deselect a row when the checkbox is checked/unchecked
				$('#simple-table').on('click', 'td input[type=checkbox]' , function(){
					var $row = $(this).closest('tr');
					if($row.is('.detail-row ')) return;
					if(this.checked) $row.addClass(active_class);
					else $row.removeClass(active_class);
				});
			
				
			
				/********************************/
				//add tooltip for small view action buttons in dropdown menu
				$('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});
				
				//tooltip placement on right or left
				function tooltip_placement(context, source) {
					var $source = $(source);
					var $parent = $source.closest('table')
					var off1 = $parent.offset();
					var w1 = $parent.width();
			
					var off2 = $source.offset();
					//var w2 = $source.width();
			
					if( parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2) ) return 'right';
					return 'left';
				}
				
				
				
				
				/***************/
				$('.show-details-btn').on('click', function(e) {
					e.preventDefault();
					$(this).closest('tr').next().toggleClass('open');
					$(this).find(ace.vars['.icon']).toggleClass('fa-angle-double-down').toggleClass('fa-angle-double-up');
				});
				/***************/
				
				
				
				
				
				/**
				//add horizontal scrollbars to a simple table
				$('#simple-table').css({'width':'2000px', 'max-width': 'none'}).wrap('<div style="width: 1000px;" />').parent().ace_scroll(
				  {
					horizontal: true,
					styleClass: 'scroll-top scroll-dark scroll-visible',//show the scrollbars on top(default is bottom)
					size: 2000,
					mouseWheelLock: true
				  }
				).css('padding-top', '12px');
				*/
			
			
			})
		</script>
	</body>
</html>
