<?php
include('config.php');
$msg='';
if(isset($_POST['submit'])) {
	$admin_id = $_POST['admin_id'];
	$birthday = $_POST['birthday'];
	$query = "select * from `admin_login` where `admin_id`='$admin_id' AND `birthday`='$birthday'";
	$result = mysqli_query($con,$query);
	if(mysqli_num_rows($result)>0){
		$admin=mysqli_fetch_assoc($result);
		$_SESSION['admin_sess']=$admin_id;
		header("location:index.php");
	}else{
		$msg = "<div style='background-color:red; height:6%; line-height:2; color:#fff; font-weight:bold; font-size:18px; width:60%;'> Id  OR DOB is Incorect</div>";
	}
}
?>


<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Login</title>

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="assets/css/form-elements.css">
        <link rel="stylesheet" href="assets/css/style.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="assets/ico/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">

    </head>

    <body>

        <!-- Top content -->
        <div class="top-content">
        	
            <div class="inner-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2 text">
                            <h1>Login Form</h1>
							
                            <div class="description">
                            	
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3 form-box">
                        	<div class="form-top">
                        		<div class="form-top-left">
                        			<h3>Login Form</h3>
									
                            		<p>Enter your ID and Birth Date to log on:</p>
                        		</div>
                        		<div class="form-top-right">
                        			<i class="fa fa-lock"></i>
                        		</div>
                            </div>
							
                            <div class="form-bottom">
							 <?php if($msg!=''){ echo $msg; } ?>
						
			                    <form action="" method="post" class="login-form">
			                    	<div class="form-group">
			                    		<label class="sr-only" for="form-username">Admin ID</label>
			                        	<input type="text" name="admin_id" placeholder="Enter Your Id" class="form-username form-control" id="form-username">
			                        </div>
			                        <div class="form-group">
			                        	<label class="sr-only" for="form-password">Date of Birth</label>
			                        	<input type="text" name="birthday" placeholder="Date of Birth" class="form-password form-control" id="form-password">
			                        </div>
									<div class="form-group">
									 <button class="btn btn-danger btn-block form_btn_"
									type="submit" name="submit" class="btn">Sign In!</button><br><br>
			                       
	  
									</div>
								 </div><br>
        <span class="bigger-120">
							<span class="blue bolder">Tom Wininger & Rotem Zecharya</span>
							Application &copy; 2016-2017
						</span>
			                    </form>
		                    </div>
                        </div>
                    </div>
                  
                </div>
            </div>
            
        </div>


        <!-- Javascript -->
        <script src="assets/js/jquery-1.11.1.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/js/jquery.backstretch.min.js"></script>
        <script src="assets/js/scripts.js"></script>
        
        <!--[if lt IE 10]>
            <script src="assets/js/placeholder.js"></script>
        <![endif]-->

    </body>

</html>