<?php
include('config.php');
$msg='';
if(isset($_POST['new_account'])){
		
		$name = $_POST['name'];
		$role = $_POST['role'];
		$user_id = $_POST['user_id'];
		$date_birth = $_POST['date_birth'];
		
		$query = "select * from `Priority_users` where `user_id`='$user_id'";
		$result = mysqli_query($con,$query);
		
		if(mysqli_num_rows($result)>0)
		{
			$msg = "<div style='background-color:red; width:40%; height:6%; line-height:2; text-align:center; color:#fff; margin-bottom:10px; margin-left:450px;'>This User ID Is Already Exisit</div>";
		}else{
		
		$query = "INSERT INTO `Priority_users`(`name`,`role`,`user_id`,`date_birth`) VALUES ('$name','$role','$user_id','$date_birth')";	
		if(mysqli_query($con, $query)){
			$msg = "<div class='alert alert-success'>Employee has successfully been Registered.</div>";
			header("location:login.php");
		}else{
			$msg = "<div class='alert alert-danger'>Some Error Occure!</div>";
		}
	}	
	}
?>







<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Registration</title>

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="assets/css/form-elements.css">
        <link rel="stylesheet" href="assets/css/style.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="assets/ico/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">

    </head>

    <body>

        <!-- Top content -->
        <div class="top-content">
        	
            <div class="inner-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2 text">
                            <h1><strong></strong>  Form Registration</h1>
                            <div class="description">
                            	
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3 form-box">
                        	<div class="form-top">
                        		<div class="form-top-left">
                        			<h1 class="text-center create_">Create Account</h1>
                            	
                        		</div>
                        		
                            </div>
                            <div class="form-bottom">
			                    <form role="form" action="" method="post" class="login-form">
								
									 <?php if($msg!=''){ echo $msg; } ?>
									 
			                        <div class="form-group">
			                        	<label class="sr-only" for="form-password">Name</label>
			                        	<input type="text" name="name" placeholder="Enter Name.." class="form-password form-control" id="form-password">
			                        </div>
									
			                        <div class="form-group">
			                        	<label class="sr-only" for="form-password">Role</label>
			                        	<input type="text" name="role" placeholder="Enter Role.." class="form-password form-control" id="form-password">
			                        </div>
									
			                    	<div class="form-group">
			                    		<label class="sr-only" for="form_username">User ID</label>
			                        	<input type="text" name="user_id" placeholder="Enter User ID..." class="form-username form-control" id="form_username">
			                        </div>
			                        <div class="form-group">
			                        	<label class="sr-only" for="form-password">Date of Birth</label>
			                        	<input type="text" name="date_birth" placeholder="Date of Birth" class="form-password form-control" id="form-password">
			                        </div>
			                        <button type="submit" class="btn" name="new_account">Create Account!</button>
			                    </form>
		                    </div>
                        </div>
                    </div>
                  
                </div>
            </div>
            
        </div>


        <!-- Javascript -->
        <script src="assets/js/jquery-1.11.1.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/js/jquery.backstretch.min.js"></script>
        <script src="assets/js/scripts.js"></script>
        
        <!--[if lt IE 10]>
            <script src="assets/js/placeholder.js"></script>
        <![endif]-->

    </body>

</html>