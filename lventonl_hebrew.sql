-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 04, 2017 at 11:35 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lventonl_hebrew`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_login`
--

CREATE TABLE `admin_login` (
  `id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `birthday` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_login`
--

INSERT INTO `admin_login` (`id`, `admin_id`, `birthday`) VALUES
(1, 4545, '1989');

-- --------------------------------------------------------

--
-- Table structure for table `Priority_users`
--

CREATE TABLE `Priority_users` (
  `id` int(11) NOT NULL,
  `name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `role` varchar(200) CHARACTER SET utf8 NOT NULL,
  `user_id` int(11) NOT NULL,
  `date_birth` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Priority_users`
--

INSERT INTO `Priority_users` (`id`, `name`, `role`, `user_id`, `date_birth`) VALUES
(6, 'sehar', 'CFL Manager', 65456, '2535'),
(7, 'jdsjhb', 'oked Manager', 545120, '1545'),
(9, 'תום', 'CFL Dep Manager', 11, '1234'),
(10, 'aj', 'Rokahat Moked', 45, '789'),
(11, 'hbk', 'Nurse Manager', 456, '1231'),
(12, 'rko', 'Data Manager', 33, '354'),
(13, 'waqar', 'Representative', 66, '666'),
(14, 'hani', 'Field Nurse', 777, '8888');

-- --------------------------------------------------------

--
-- Table structure for table `PARY_LOADWORKERGRADE`
--

CREATE TABLE `PARY_LOADWORKERGRADE` (
  `id` int(11) NOT NULL,
  `IDNUMBER` varchar(11) NOT NULL,
  `PACKAGEID` varchar(30) NOT NULL,
  `GRADE` varchar(150) NOT NULL,
  `TIME_PERIOD` varchar(250) NOT NULL,
  `update` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `PARY_LOADWORKERGRADE`
--

INSERT INTO `PARY_LOADWORKERGRADE` (`id`, `IDNUMBER`, `PACKAGEID`, `GRADE`, `TIME_PERIOD`, `update`) VALUES
(29, '11', '148', '100%', '12', '2017-05-04 17:24:17'),
(30, '11', '150', '100%', '12', '2017-05-07 11:36:39'),
(31, '11', '151', '50%', '3', '2017-05-25 05:11:35'),
(32, '11', '152', '0%', '3', '2017-05-25 05:32:04'),
(34, '11', '156', '100%', '12', '2017-05-25 06:00:11'),
(35, '11', '157', '100%', '12', '2017-05-27 08:52:34'),
(36, '11', '159', '50%', '3', '2017-06-04 21:31:07'),
(37, '11', '158', '0%', '3', '2017-06-04 21:31:18');

-- --------------------------------------------------------

--
-- Table structure for table `heb`
--

CREATE TABLE `heb` (
  `id` int(11) NOT NULL,
  `ced` varchar(200) NOT NULL,
  `erg` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `result`
--

CREATE TABLE `result` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `result` varchar(30) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pkg`
--

CREATE TABLE `tbl_pkg` (
  `id` int(11) NOT NULL,
  `name_work` varchar(200) NOT NULL,
  `image` varchar(200) CHARACTER SET latin1 NOT NULL,
  `b_basic` varchar(200) CHARACTER SET latin1 NOT NULL,
  `b_ceo` varchar(200) CHARACTER SET latin1 NOT NULL,
  `b_hop` varchar(200) CHARACTER SET latin1 NOT NULL,
  `b_hrad_HR` varchar(200) CHARACTER SET latin1 NOT NULL,
  `b_sm` varchar(200) CHARACTER SET latin1 NOT NULL,
  `b_itm` varchar(200) CHARACTER SET latin1 NOT NULL,
  `b_pw` varchar(200) CHARACTER SET latin1 NOT NULL,
  `b_p2` varchar(200) CHARACTER SET latin1 NOT NULL,
  `CFL_MANAGER` varchar(200) CHARACTER SET latin1 NOT NULL,
  `MOKED_MANAGER` varchar(200) CHARACTER SET latin1 NOT NULL,
  `CFL_DEP_MANAGER` varchar(200) CHARACTER SET latin1 NOT NULL,
  `ROKAHAT_MOKED` varchar(200) CHARACTER SET latin1 NOT NULL,
  `NURSE_MANAGER` varchar(200) CHARACTER SET latin1 NOT NULL,
  `DATA_MANAGER` varchar(200) CHARACTER SET latin1 NOT NULL,
  `REPRESENTATIVE` varchar(200) CHARACTER SET latin1 NOT NULL,
  `FIELD_NURSE` varchar(200) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pkgs`
--

CREATE TABLE `tbl_pkgs` (
  `id` int(11) NOT NULL,
  `logo_id` int(11) NOT NULL,
  `que` varchar(200) CHARACTER SET utf8 NOT NULL,
  `que_radio` varchar(200) NOT NULL,
  `a_1` varchar(200) CHARACTER SET utf8 NOT NULL,
  `a_2` varchar(200) CHARACTER SET utf8 NOT NULL,
  `a_3` varchar(200) CHARACTER SET utf8 NOT NULL,
  `a_4` varchar(200) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_login`
--
ALTER TABLE `admin_login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Priority_users`
--
ALTER TABLE `Priority_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `PARY_LOADWORKERGRADE`
--
ALTER TABLE `PARY_LOADWORKERGRADE`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `heb`
--
ALTER TABLE `heb`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `result`
--
ALTER TABLE `result`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_pkg`
--
ALTER TABLE `tbl_pkg`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_pkgs`
--
ALTER TABLE `tbl_pkgs`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_login`
--
ALTER TABLE `admin_login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `Priority_users`
--
ALTER TABLE `Priority_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `PARY_LOADWORKERGRADE`
--
ALTER TABLE `PARY_LOADWORKERGRADE`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `heb`
--
ALTER TABLE `heb`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `result`
--
ALTER TABLE `result`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT for table `tbl_pkg`
--
ALTER TABLE `tbl_pkg`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=160;
--
-- AUTO_INCREMENT for table `tbl_pkgs`
--
ALTER TABLE `tbl_pkgs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=204;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
